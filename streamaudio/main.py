import numpy as np
import pyaudio


p = pyaudio.PyAudio()

# Samples per second based on a CD
samples_per_second = 44100

# Duration in seconds
duration_seconds = 2.5

freqency_hertz = 261.63

# Create initial array before calculations
number_of_samples = np.arange(duration_seconds * samples_per_second)

# Apply sin wave fucntion over the array
sine_wave_array = np.sin(2 * np.pi * number_of_samples * freqency_hertz / samples_per_second)

# Use the right data within the right region (normalization)
sine_wave_array_integers = np.int16(sine_wave_array * 32767)



stream = p.open(format = pyaudio.paFloat32, channels = 1, rate = samples_per_second, output=True)
stream.write(sine_wave_array_integers)
stream.close()
p.terminate()
print("yep")