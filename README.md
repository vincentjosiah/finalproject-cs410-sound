I created this program as my final project for my CS 410 TOP: COMPUTERS, SOUND & MUSIC.  This was my first class ever working with audio and it was a blast!

This program will play a full octave of notes from middle C to the C in the next octave using keys C D E F G A B Z (where Z is the C in the higher octave). 

It is an extremely simple program when i stand back and look at it, but believe it or not it took hours of research and lots of silence when there should be sound!  It was worth the journey!  Really helped me appreciate the effort that goes into making electronic sound software.



NOTE: To run the program it is important to note that this uses pyaudio, which is a bit difficult to get installed.  Below are the commands that i found useful to get pyaudio working.

sudo apt-get install libasound-dev portaudio19-dev libportaudio2 libportaudiocpp0
sudo apt-get install ffmpeg
sudo pip3 install pyaudio