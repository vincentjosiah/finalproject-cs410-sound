import numpy as np
from scipy.io.wavfile import write
import os
import pyaudio

def CreateStream(freqency_hertz, stream):
	# Samples per second based on a CD
	samples_per_second = 44100

	# Duration in seconds
	duration_seconds = 2.0

	# Create initial array before calculations
	number_of_samples = np.arange(duration_seconds * samples_per_second)

	# Apply sin wave fucntion over the array
	sine_wave_array = np.sin(2 * np.pi * number_of_samples * freqency_hertz / samples_per_second)

	# Use the right data within the right region (normalization)
	sine_wave_array_integers = np.int16(sine_wave_array * 32767)

	stream.write(sine_wave_array_integers)

# Part of my old design.  Very similar though to the function above, but the function above uses a stream instead of creating wav files.
# def createSineWave(freqency_hertz, filename):
# 	# Samples per second based on a CD
# 	samples_per_second = 44100

# 	# Duration in seconds
# 	duration_seconds = 0.3

# 	# Create initial array before calculations
# 	number_of_samples = np.arange(duration_seconds * samples_per_second)

# 	# Apply sin wave fucntion over the array
# 	sine_wave_array = np.sin(2 * np.pi * number_of_samples * freqency_hertz / samples_per_second)

# 	# Reduce the volume to 1/3 because LOUD NOISES
# 	quieter_sine_wave_array = sine_wave_array * 0.3

# 	# Use the right data within the right region (normalization)
# 	sine_wave_array_integers = np.int16(sine_wave_array * 32767)

# 	write(filename, 44100, sine_wave_array_integers)

# This was my original route.  I was struggling to get pyaudio working, but once I got it working i went a stream route so i didn't have to create all the files each time.
# I did find using wav files gives you a slightly different sound.

# def initializeWaves():
# 	c_note = createSineWave(261.63, 'c_wave.wav')

# 	d_note = createSineWave(293.66, 'd_wave.wav')

# 	e_note = createSineWave(329.63, 'e_wave.wav')

# 	f_note = createSineWave(349.23, 'f_wave.wav')

# 	g_note = createSineWave(392.0, 'g_wave.wav')

# 	a_note = createSineWave(440.0, 'a_wave.wav')

# 	b_note = createSineWave(493.88, 'b_wave.wav')

# 	z_note = createSineWave(523.25, 'z_wave.wav')

# def remove():
# 	os.remove("c_wave.wav")
# 	os.remove("d_wave.wav")
# 	os.remove("e_wave.wav")
# 	os.remove("f_wave.wav")
# 	os.remove("g_wave.wav")
# 	os.remove("a_wave.wav")
# 	os.remove("b_wave.wav")
# 	os.remove("z_wave.wav")