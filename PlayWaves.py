import sys
import termios
import tty
import os
import time
#import playsound
import CreateWaves
import pyaudio

#used for input to make things a little cleaner
def getch():
	#Returns a single character from standard input
	fd = sys.stdin.fileno()
	old_settings = termios.tcgetattr(fd)
	try:
		tty.setraw(sys.stdin.fileno())
		ch = sys.stdin.read(1)
	finally:
		termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
	return ch


# displays the main menu and calls the basic wav functions
def menu():
	button_delay = 0.5

	p = pyaudio.PyAudio()
	stream = p.open(format = pyaudio.paFloat32, channels = 1, rate = 44100, output=True)

	print("Welcome BASICSYNTH\n\n")

	print("Press Q to quit the application")
	print("You can press C, D, E, F, G, A, B, Z to play a full octave of notes (c being middle c up to the c in the next octave")

	while(True):
		char = getch()

		if(char == "q"):
			print("Application Quitting")
			break
		elif(char == "c"):
			#playsomething("c_wave.wav")
			playstream(261.63, stream)
		elif(char == "d"):
			#playsound.playsound('d_wave.wav')
			playstream(293.66, stream)
		elif(char == "e"):
			#playsound.playsound('e_wave.wav')
			playstream(329.63, stream)
		elif(char == "f"):
			#playsound.playsound('f_wave.wav')
			playstream(349.23, stream)
		elif(char == "g"):
			#playsound.playsound('g_wave.wav')
			playstream(392.0, stream)
		elif(char == "a"):
			#playsound.playsound('a_wave.wav')
			playstream(440.0, stream)
		elif(char == "b"):
			#playsound.playsound('b_wave.wav')
			playstream(493.88, stream)
		elif(char == "z"):
			#playsound.playsound('z_wave.wav')
			playstream(523.25, stream)
		else:
			print("Sorry that is not a valid input")
			print("Press Q to quit the application")
			print("You can press C, D, E, F, G, A, B, Z to play a full octave of notes (c being middle c up to the c in the next octave")	
	stream.close()
	p.terminate()

# Originally i needed this, but went a different route.
# def playsomething(file):
# 	playsound.playsound(file)

def playstream(frequency, stream):
	CreateWaves.CreateStream(frequency, stream)